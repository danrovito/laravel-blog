<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class BlogPostController extends Controller {

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::all();

        return View('posts', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('blogpostcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $post = new Post;

        $post->title = Input::get('title');
        $post->slug = Input::get('slug');
        $post->draft = Input::get('draft');
        $post->excerpt = Input::get('excerpt');
        $post->body = Input::get('body');
        $post->author = Auth::user()->name;

        $post->save();

        return Redirect::to('/blog-post');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return View('blogpostedit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $post = Post::find($id);

        $post->title = Input::get('title');
        $post->slug = Input::get('slug');
        $post->draft = Input::get('draft');
        $post->excerpt = Input::get('excerpt');
        $post->body = Input::get('body');
        $post->author = Auth::user()->name;

        $post->save();

        return Redirect::to('/blog-post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Post::destroy($id);

        return Redirect::to('/blog-post');
    }

}