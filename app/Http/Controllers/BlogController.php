<?php namespace App\Http\Controllers;
use App\Post;

class BlogController extends Controller {

    public function index()
    {
        $posts = Post::where('draft', '=', 0)->orderBy('created_at', 'desc')->get();        

        return View('index')->with('posts', $posts);
    }

}