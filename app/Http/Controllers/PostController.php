<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;
class PostController extends Controller {
    public function getShow($slug)
    {
        $post = Post::where('slug', '=', $slug)->firstOrFail();
        return View('postsshow')-> with('post', $post);
    }
}