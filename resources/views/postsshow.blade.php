@extends('app')
@section('title')
{!! $post->title !!}
@endsection
@section('description')
{!! $post->excerpt !!}
@endsection
@section('content')    
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-546e25535f17d225" async="async"></script>
        <div class="container">
            <div class="">
                <div class="col-md-9">
                    <article>
                        <h2>{!! $post->title !!}</h2>
                        <p>Published On {!! $post->created_at->format('l F jS Y') !!} </p>
                        <div class="post-body">
                        {!! $post->body !!}
                        </div>
                    </article>
                    <div class="share">
                        <h3>Share:</h3>
                        <div class="addthis_sharing_toolbox"></div>
                    </div>
                    <!--Comments-->
                    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'danrovito';
        
        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                </div>
            </div>
        </div>
@endsection
@section('sidebar')
@include('partials.sidebar')
@endsection