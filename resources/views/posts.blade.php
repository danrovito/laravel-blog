@extends('app')
@section('content')    
<div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="/blog-post/create" class="btn btn-success">Add Post</a>
            </div>
        </div>
    </div>
        <div class="row">
            <table class="table table-bordered table-striped" id="posts">

                <thead>
                <tr>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Draft</th>
                    <th>Author</th>
                    <th>Date/Time Created</th>
                    <th>Controls</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->slug }}</td>
                        <td>{{ $post->draft }}</td>
                        <td>{{ $post->author }}</td>
                        <td>{{ $post->created_at->format('F d, Y h:ia') }}</td>
                        <td>
                        <a href="/blog-post/{{ $post->id }}/edit" class="btn btn-info btn-sm pull-left" style="margin-right: 3px;"><i class="fa fa-pencil"></i></a>
                        {!! Form::open(['url' => '/blog-post/' . $post->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete?")']) !!}
                        {!! Form::button('<i class="fa fa-times"></i>', array(
                            'class'=>'btn btn-danger btn-sm',
                            'type'=>'submit', 'title' => 'Delete')) 
                            !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
@endsection
@section('scripts')
<link rel="stylesheet" href="//cdn.aircss.io/common/jquery.dynatable.css">
<script src="//cdn.aircss.io/aircss/jquery.dynatable.js"></script>
<script>
    $(document).ready( function () {
        $('#posts').dynatable();
    } );
</script>
@endsection