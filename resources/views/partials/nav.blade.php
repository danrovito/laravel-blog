<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">DANROVITO.COM</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/' ? ' active' : '');?>"><a href="/">Blog</a></li>
        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/projects' ? ' active' : '');?>"><a href="/projects">Projects</a></li>  
        <!--<li class="<?php echo ($_SERVER['REQUEST_URI'] == '/podcast' ? ' active' : '');?>"><a href="#">Podcast</a></li> 
        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/newsletter' ? ' active' : '');?>"><a href="#">Newsletter</a></li>   -->    
      </ul>        
    </div>
  </div>
</nav>