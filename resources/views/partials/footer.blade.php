<footer class="footer">
      <div class="container">
        <p class="text-muted">&nbsp;©&nbsp;<?php echo date("Y") ?> DANROVITO.COM | <a href="https://twitter.com/danrovito">@danrovito</a></p>
      </div>
    </footer>