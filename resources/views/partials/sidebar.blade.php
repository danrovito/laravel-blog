<div class="col-md-3 sidebar">
    <img class="me-pic" src="https://danrovito.com/img/dr-green.png">
    <p>Hi, I'm <strong>Dan Rovito</strong></p>
    <p>I'm the Founder of <a href="https://www.ohiovalleyphp.com">Ohio Valley PHP</a></p>
    <p>You can connect with me on Twitter <a href="https://twitter.com/danrovito">@DanRovito</a></p>
    <br>
    <div>
    <a href="https://twitter.com/DanRovito" class="twitter-follow-button" data-show-count="true">Follow @DanRovito</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    </div>
  
</div>