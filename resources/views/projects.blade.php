@extends('app')
@section('title')
Projects
@endsection
@section('description')
I am always looking for new and exciting projects to work on.  Here are some of the projects I've worked on recently
@endsection
@section('content')
<h1>Projects</h1>
<p>I am always looking for new and exciting projects to work on.  Here are some of the projects I've worked on recently:</p>
<ul>
<li><a href="https://aircss.io">AirCSS</a></li>
  <li><a href="https://www.cloudstride.net">CloudStride.net</a></li>
  <li>ClientList.co (In early development)</li>
</ul>
@endsection
@section('sidebar')
@include('partials.sidebar')
@endsection
