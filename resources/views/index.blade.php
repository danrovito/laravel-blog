@extends('app')
@section('title')
Hi, I'm Dan Rovito
@endsection
@section('description')
I'm the founder of Ohio Valley PHP and the creator of AirCSS.io. You can connect with me on Twitter @DanRovito
@endsection
@section('content')  
  <div class="container">
<div class="blog-box">
            <div class="">
                <div class="col-md-9">
    <div>
    @if($posts->count() == 0)
    <p>No Post</p>    
    @elseif($posts->count())
        @foreach($posts as $post)
            <article>
            <div class="title">
                <h3><a href="{!! URL::route('post-show', $post->slug) !!}">{!! $post->title !!}</a></h3>
                </div>
                <p><small>Published On {!! $post->created_at->format('l F jS Y') !!} by <strong>{!! $post->author !!}</strong></small></p>
                {!! $post->excerpt !!}
                <div class="read-more-top"><a class="read-more" href="{!! URL::route('post-show', $post->slug) !!}">Read More &rarr;</a></div>
            </article>
            <hr/>
        @endforeach

    @endif
                  </div>
              </div>
  </div>
    </div>
</div>
@endsection
@section('sidebar')
@include('partials.sidebar')
@endsection