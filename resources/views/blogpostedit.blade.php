@extends('app')

@section('content')    

<div class='col-lg-12'>

        @if ($errors->has())
            @foreach ($errors->all() as $error)
                <div class='bg-danger alert'>{{ $error }}</div>
            @endforeach
        @endif

        <h1><i class='fa fa-user'></i> Edit Post</h1><span><a href="/upload" target="_blank">Upload Image</a></span>

        {!! Form::model($post, ['role' => 'form', 'url' => '/blog-post/' . $post->id, 'method' => 'PUT']) !!}

            <div class='form-group'>
                {!! Form::label('title', 'Title') !!}
                {!! Form::text('title', null, ['placeholder' => 'Title', 'class' => 'form-control']) !!}
            </div>

            <div class='form-group'>
                {!! Form::label('slug', 'Slug') !!}
                {!! Form::text('slug', null, ['placeholder' => 'Slug', 'class' => 'form-control']) !!}
            </div>

            <div class='form-group'>
                {!! Form::label('draft', 'Draft') !!}
                <div>
                    Yes&nbsp;{!! Form::radio('draft', '1', ['class' => 'form-control']) !!}
                    No&nbsp;{!! Form::radio('draft', '0',['class' => 'form-control']) !!}
                </div>
            </div>
  
            <div class='form-group'>
            {!! Form::label('excerpt', 'Excerpt') !!}
            {!! Form::textarea('excerpt', null, [ 'class' => 'form-control']) !!}
            </div>

            <div class='form-group'>
                {!! Form::label('body', 'Post') !!}
                {!! Form::textarea('body', null, [ 'class' => 'form-control', 'id' => 'bodyinput']) !!}
            </div>

            <div class='form-group'>
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}

    </div>

@endsection
